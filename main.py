import os
import sys

from PySide2.QtCore import QFile, QAbstractListModel, Qt, QPoint
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QApplication, QMainWindow, QStyledItemDelegate, QWidget, QListWidgetItem, QLabel, QPushButton
from PySide2.QtGui import QColor, QRegion


widget_file = os.path.join(os.path.realpath(os.path.dirname(__file__)), 'widgets', 'list_entry.ui')


def load_ui(file_name, where=None):
    """
    Loads a .UI file into the corresponding Qt Python object
    :param file_name: UI file path
    :param where: Use this parameter to load the UI into an existing class (i.e. to override methods)
    :return: loaded UI
    """
    # Create a QtLoader
    loader = QUiLoader()
    # Open the UI file
    ui_file = QFile(file_name)
    ui_file.open(QFile.ReadOnly)
    # Load the contents of the file
    ui = loader.load(ui_file, where)
    # Close the file
    ui_file.close()
    return ui


class ListDelegate(QStyledItemDelegate):
    def __init__(self, parent=None):
        super().__init__(parent)

    def paint(self, painter, option, index):
        original_paint_device = painter.device()
        painter.begin(painter.device())
        #painter.setBrush(QColor(255, 0, 0, 255))
        #painter.drawRect(option.rect)
        widget = load_ui(widget_file)
        widget.setGeometry(option.rect)
        painter.end()
        widget.render(
            painter.device(),
            QPoint(option.rect.x(), option.rect.y()),
            QRegion(0, 0, option.rect.width(), option.rect.height()),
            QWidget.RenderFlag.DrawChildren)

    def sizeHint(self, option, index):
        return QStyledItemDelegate.sizeHint(self, option, index)


class ListModel(QAbstractListModel):
    def __init__(self, *args, parent, list_view, entries=None, **kwargs):
        super().__init__(parent)
        self.entries = entries or []
        self.list_view = list_view

    def data(self, index, role):
        if role == Qt.DisplayRole:
            # See below for the data structure.
            text = self.entries[index.row()]['text']
            # Return the todo text only.

            return text

    def rowCount(self, index):
        return len(self.entries)


class CustomListWidgetItem(QListWidgetItem):
    def __init__(self, data):
        super().__init__()
        self.widget = load_ui(widget_file)
        self.title = data['title']
        self.year = data['year']
        self.rating = data['rating']
        self.widget.label.setText(self.year)
        self.widget.label_2.setText(self.title)
        self.widget.label_3.setText(self.rating)



class MyMainWindow(QMainWindow):
    def __init__(self, parent=None):
        super().__init__()
        ui_file_path = os.path.join(os.path.realpath(os.path.dirname(__file__)), 'gui.ui')
        self.test_entries = [(False, 'an item'), (False, 'another item')]
        self.dict_entries = [{'title': 'this is a test', 'year': '2012', 'rating': '8.1'}, {'title': 'blubb', 'year': '1994', 'rating': '7.4'}, {'title': 'matrix', 'year': '1999', 'rating': '8.9'}]
        self.gui = load_ui(ui_file_path)

        self.gui.listWidget.addItem(QListWidgetItem('test'))
        self.gui.listWidget.addItem(QListWidgetItem('test2'))
        self.gui.listWidget.addItem(QListWidgetItem('test3'))
        self.gui.listWidget.addItem(QListWidgetItem('test4'))
        self.gui.listWidget.addItem(QListWidgetItem('test5'))

        for entry in self.dict_entries:
            self.item_for_listWidget = CustomListWidgetItem(entry)
            # Set Size for item otherwise it wouldn't show
            self.item_for_listWidget.setSizeHint(self.item_for_listWidget.widget.sizeHint())
            self.item_for_listWidget.setFlags(Qt.ItemIsSelectable)
            self.gui.listWidget.addItem(self.item_for_listWidget)
            self.gui.listWidget.setItemWidget(self.item_for_listWidget, self.item_for_listWidget.widget)
        print(self.gui.listWidget.count())
        print(self.gui.listWidget.selectedItems())
            #test_widget.label.setText(entry['text'])
            #self.gui.listWidget.addItem(list_widget_item)
            #print(list_widget_item.listWidget())
        #self.gui.listView.setItemDelegate(ListDelegate())
        #self.model = ListModel(entries=self.dict_entries, parent=self, list_view=self.gui.listView)
        #self.gui.listView.setModel(self.model)
        self.gui.lineEdit.setText("test")


if __name__ == "__main__":
    # Create Qt app
    app = QApplication(sys.argv)
    # Load ui and show the widget
    window = MyMainWindow()
    window.gui.show()
    # Run the app
    sys.exit(app.exec_())
